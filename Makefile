.PHONY:build run clean clean-all check

build:
	docker build -t master-tgv .

run:
	docker run -p 5000:5000/tcp -p 5000:5000/udp -ti --name ttt master-tgv

clean:
	docker container prune

clean-all:
	docker system prune

check:
	python test/testsuite.py
