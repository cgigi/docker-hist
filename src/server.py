from flask import Flask, request, jsonify, make_response

import histo
import json
import shutil
import subprocess
import tempfile

app = Flask(__name__)
# set the maximum length of a file
app.config['MAX_CONTENT_LENGTH'] = 1000 * 1024 * 1024  # 1 Gb


@app.errorhandler(413)
def internal_error(error):
    """Error handling for 413 Flask error (file too big)."""
    print("\033[31m[ERROR] File too big!\033[0m")
    error = {
            "error": {
             "code": 413,
             "message": "The file is too big!"
             }
            }
    return make_response(jsonify(error), 413)


@app.route('/histogram', methods=['POST'])
def histo():
    """Compute histogram and return a json with values."""
    if request.method == "POST":
        print("\033[35m[INFO] Get a POST request\033[0m")

        # get the image
        name = request.files["image"]

        # create the tmp directory
        tempdir = tempfile.mkdtemp()
        print("\033[35m[INFO] Create a new directory: %s\033[0m" % (tempdir))

        # save the file in tmp directory created before
        name.save("%s/sncf.png" % (tempdir))

        # try catch to handle if the file is not an image
        try:
            subprocess.check_output(["./src/histo.py", "--output", tempdir + "/sncf.json", tempdir + "/sncf.png"])
        except subprocess.CalledProcessError:

            # this is not an image, return an error!
            print("\033[31m[ERROR] File is not valid!\033[0m")
            error = {
                    "error": {
                     "code": 401,
                     "message": "The file type is not an image!"
                     }
                    }
            return make_response(jsonify(error), 400)

        # all good create the json
        json_data = json.load(open(tempdir + "/sncf.json"))

        # clean the docker
        print("\033[35m[INFO] Remove the directory\033[0m")
        shutil.rmtree(str(tempdir))

        # return the json result
        return make_response(jsonify(json_data), 200)
    return f'Hello'
