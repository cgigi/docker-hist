#!/usr/bin/env python3

import numpy as np
import argparse
import json

from PIL import Image
from matplotlib import pyplot as plt


def main():
    parser = argparse.ArgumentParser(
        description="Calcul an histogram for each band of the image"
    )
    parser.add_argument("image", help="input image")
    parser.add_argument("--output", metavar="FILE", help="result output file")
    parser.add_argument(
        "--plot", action="store_true", help="show a plot of the histogram to debug"
    )
    args = parser.parse_args()

    img = Image.open(args.image)
    hist = {}

    bands = img.getbands()
    for ch, band in enumerate(bands):
        hist_item, bins = np.histogram(img.getdata(ch), 256, [0, 256])
        hist[ch] = hist_item.tolist()
        if args.plot:
            plt.plot(hist_item, band.lower())
            plt.xlim([0, 256])

    if args.output:
        with open(args.output, "w") as fh:
            json.dump(hist, fh)
    else:
        print(hist)

    if args.plot:
        plt.show()


if __name__ == "__main__":
    main()
