FROM python:3.7-slim

# Setting dir
WORKDIR /home/

# Install package
COPY requirements.txt /
RUN pip3 install -r /requirements.txt

# Add python scripts
RUN mkdir src/
ADD src/server.py src/.
ADD src/histo.py src/.

# Setting environment variables
ENV FLASK_APP=src/server.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# Prepare the run
CMD flask run
