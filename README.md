# docker-hist

* AUTHOR: Charles 'master-TGV' Ginane
* MAIL: charles.ginane@gmail.com
* VERSION: 1.0

## Purpose

This project is to create a docker with a web interface. We can interact with the we interface with an API REST to compute the histogram of an image


## Architecture

```
.
| docker-compose.yml # It's the docker-compose configuration file
| Dockerfile         # Docker creating image file
| Makefile           # Help to build and lauch the docker without docker-compose
| README.md          # This file
| requirements.txt   # All Python packages needed for the docker
| src/               # Scripts needed for the Flask server
| test/              # Tests and testsuite location
| test_img/          # Location of test images
```

## Configuration

* Docker
* docker-compose
* python>=3.6

## Step 1 - Build and run the project

### With docker-compose

You can use the docker-compose.yml to build and run the docker. You have just to launch the following command
```
> docker-compose up
```

### With the Makefile

A Makefile is provided with the project.
To build the project, you just have to launch the following command

```
> make build
```

This command creates the image named master-TGV based on python:3.7-slim

And to run the docker.

```
> make run
```

This command runs a container named ttt on with a port fowarding of 5000

### The hard way

//TODO

## Step 2 - interact

To interact with the docker server interface. You just have to do a POST request at the address **http://0.0.0.0:5000/histogram** with your image

The server compute the histogram of the image and return you a JSON with the histogram for each channel.

**WARNING:** Limit size of image is 1Go. Server sends back a 413 error if the image is too big!

## (BONUS) Step 3 - test the project

A testsuite is provided with the project in the directory `test/`. You can launch it with those commands

```
make check
or
python test/testsuite.py
```

## (BONUS) Step 4 - Clean the docker:

//TODO
